
Changelog
=========

..
   All enhancements and patches to minimal-activitypub will be documented
   in this file.  It adheres to the structure of http://keepachangelog.com/ ,
   but in reStructuredText instead of Markdown (for ease of incorporation into
   Sphinx documentation and the PyPI description).

   This project adheres to Semantic Versioning (http://semver.org/).

Unreleased
----------------

See the fragment files in the `changelog.d directory`_.

.. _changelog.d directory: https://codeberg.org/MarvinsMastodonTools/minimal-activitypub/src/branch/main/changelog.d


.. scriv-insert-here

.. _changelog-1.3.1:

1.3.1 — 2024-12-15
==================

Changed
-------

- Updated dependencies versions and CI setup

.. _changelog-1.3.0:

1.3.0 — 2024-11-03
==================

Added
-----

- Added mastodon API method `Perform a search`_  - `search` method.

.. _Perform a search: https://docs.joinmastodon.org/methods/search/#v2

- Added mastodon API method `View hashtag timeline`_ - `get_hashtag_timeline` method

.. _View hashtag timeline: https://docs.joinmastodon.org/methods/timelines/#tag

Changed
-------

- Made `access_token` in init method optional to allow for some unauthenticated API calls such as search and hastags timeline

Fixed
-----

- Corrected setting of `visibility` value on post_status method

.. _changelog-1.2.3:

1.2.3 — 2024-10-28
==================

Changed
-------

- Updated CI workflow

Removed
-------

- Removed support for Python 3.8

Fixed
-----

- Fix 400 Bad Request: media not found from GoToSocial for posts w/o medias
  Thank you `MrClon`_ for `PR 14`_

.. _MrClon: https://codeberg.org/MrClon
.. _PR 14: https://codeberg.org/marvinsmastodontools/minimal-activitypub/pulls/14

- Fixed pytests

.. _changelog-1.2.2:

1.2.2 — 2024-08-30
==================

Changed
-------

- Updated dependencies

- Refactored internal method `_parse_next_prev` to not trigger `codelimit`_ anymore.

- Now using `uv`_ for maintaining dependencies in `pyproject.yaml`

- Now using `hatch`_ to build distribution archives and publish them to `Pypi`_

.. _codelimit: https://github.com/getcodelimit/codelimit
.. _uv: https://docs.astral.sh/uv/
.. _hatch: https://hatch.pypa.io/latest/
.. _Pypi: https://pypi.org/

Security
--------

- Forced version of certifi to be greater or equal to 2024.7.4. This should address `CVE-2024-39689`_

.. _CVE-2024-39689: https://github.com/certifi/python-certifi/security/advisories/GHSA-248v-346w-9cwc

.. _changelog-1.2.1:

1.2.1 — 2024-08-19
==================

Changed
-------

- Updated dependencies versions of CI workflow
- Allow use of Python 3.13
- Fix failing test for ratelimiting

.. _changelog-1.2.0:

1.2.0 — 2024-04-02
==================

Added
-----

- Added enum for possible visibility values for statuses.

Changed
-------

- Updated dependencies versions

.. _changelog-1.1.0:

1.1.0 — 2024-03-17
==================

Added
-----

- New method `get_public_timeline` to retrieve public timeline.

- New method `get_home_timeline` to get posts on the home timeline for the logged in user.

- Added new method `reblog` to reblog / boost a status on the logged in users timeline.

.. _changelog-1.0.0:

1.0.0 — 2024-03-16
==================

Breaking
--------

- Changed to using httpx AsyncClient for transport. An active AsyncClient needs to be passed in.
  See `documentation`_ for examples.

.. _documentation: https://marvinsmastodontools.codeberg.page/minimal-activitypub/

Added
-----

- Determine maximum attachment size, maximum status lengths and supported mime types from
  instance server

- Created test cases covering over 90% of the code.

Changed
-------

- Using importlib.metadata for version and package name.

- Using datetime instead of arrow for dates and times.

.. _changelog-0.5.11:

0.5.11 — 2023-12-09
===================

Fixed
-----

- Determining `max_attachments` value

.. _changelog-0.5.10:

0.5.10 — 2023-11-29
===================

Changed
-------

- Implemented issue "max_attachments directly from api" `#6`_

.. _#6: https://codeberg.org/MarvinsMastodonTools/minimal-activitypub/issues/6

- Updated dependencies versions

.. _changelog-0.5.9:

0.5.9 — 2023-11-26
==================

Added
-----

- `max_attachments` attribute with value determined by instance type.

Changed
-------

- Updated dependency versions
- Using `ruff format` instead of `black` for code formatting

.. _changelog-0.5.8:

0.5.8 — 2023-10-22
==================

Added
-----

- Running CI check for vulnerabilities on a weekly basis

Changed
-------

- Updated dependencies versions

Removed
-------

- "dev" and "docs" dependencies. Those are now covered within nox

.. _changelog-0.5.7:

0.5.7 — 2023-10-08
==================

Changed
-------

- Updated dependencies versions

.. _changelog-0.5.6:

0.5.6 — 2023-08-23
==================

Changed
-------

- Updated dependencies versions

Fixed
-----

- `Issue #4`_ by dealing with exceptions from aiohttp

.. _Issue #4: https://codeberg.org/MarvinsMastodonTools/minimal-activitypub/issues/4

.. _changelog-0.5.5:

0.5.5 — 2023-03-04
==================

Fixed
-----

- Fixed `issue #3`_ by no longer adding file extensions to upload file names as supplied
  mime type should be enough.

.. _issue #3: https://codeberg.org/MarvinsMastodonTools/minimal-activitypub/issues/3

.. _changelog-0.5.4:

0.5.4 — 2023-02-19
==================

Fixed
-----

- Addresses the issue that posts most often are posted with media marked as sensitive.
  (Ref Tootbot issues: `61`_ and `54`_)

.. _61: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/61
.. _54: https://codeberg.org/MarvinsMastodonTools/tootbot/issues/54

R.. _changelog-0.5.3:

0.5.3 - 2023-02-18
==================

Added
-----

- Created initial version of `documentation <https://marvinsmastodontools.codeberg.page/minimal-activitypub/>`_.
  Source in docs directory of repository.

Changed
-------

- Dependency control now using `pdm`_ and releases build and published to Pypi with `flit`_

.. _pdm: https://pdm.fming.dev/latest/
.. _flit: https://flit.pypa.io/en/latest/

Removed
-------

- Removed poetry references and rstcheck, pip-audit and safety from pre-commit checking. Documentation, pip-audit and safety will still be checked as part of CI workflow.

.. _changelog-0.5.2:

0.5.2 — 2023-02-13
==================

Added
-----

- Added default value for rate_limit_reset of 5 minutes for response processing from Takahe and Pleroma instances.
  Both Takahe and Pleroma don't seem to return rate limit headers.

Changed
-------

- Updated dependencies.

Removed
-------

- Removed work around for Takahe instances as Takahe made changes to behave like Mastodon and Pleroma when posting a status with media.
  See `Takahe issue 490` for details.

.. _Takahe issue 490: https://github.com/jointakahe/takahe/issues/490

.. _changelog-0.5.1:

0.5.1 — 2023-02-08
==================

Changed
-------

- More debug logging

- Updated dependencies

.. _changelog-0.5.0:

0.5.0 — 2023-02-04
==================

Added
-----

- Added methods needed to authenticate using an authorization code for servers that
  do not support authentication with username and password. New methods are:

  - `create_app` creates an app and returns client_id and client_secret

  - `generate_authorization_url` generates a URL to visit to obtain an authorization
     code needed to complete authorization

  - `validate_authorization_code` uses the authorization code to obtain an access
    token.

- Started working on more formal documentation. Please be aware though that documentation will be a work in progress for a while.

Changed
-------

- Updated dependencies

- Now using `ruff`_ for linting (replaces flake8 and some plugins)

.. _ruff: https://github.com/charliermarsh/ruff

- Make compatible with `takahe`_ instances

.. _takahe: https://jointakahe.org/

.. _changelog-0.4.1:

0.4.1 — 2023-01-20
==================

Added
-----

- Added .editorconfig to set editor values

- Added `interrogate`_ to pre-commit checks and as a dev dependency to check all methods, classes, and modules have a docstring

.. _interrogate: https://interrogate.readthedocs.io/

Changed
-------

- Updated dependencies

- Now using `scriv`_ to maintain CHANGELOG

.. _scriv: https://scriv.readthedocs.io

0.4.0 - 2022-11-11
==================

Added
----------------
- `undo_reblog` and `undo_favourite` methods

Changed
----------------
- Updated dependency versions
- `delete_status` now checks if we are talking to a Pleroma server and if the status is a reblog or a favourite and
  calls the respective `undo_reblog` or `undo_favourite` method instead of attempting to delete the status itself.

Breaking Changes
----------------
- Changed parameter name for `delete_status` to "status". This parameter can now be just the id of a status or a dict of a status


0.3.1 - 2022-10-21
==================

Changed
----------------
- Updated dependency versions
- Removed `rich` as a dependency as it is not actually used

0.3.0 - 2022-10-14
==================

Added
----------------
- `post_status` and `post_media` methods

Changed
----------------
- Updated dependency versions

Breaking Changes
----------------
- Changed how pagination information is stored.


0.2.1 - 2022-09-17
==================

Added
----------------
- Started project for a minimal implementation of the ActivityPub rest API used by
  `Mastodon`_ and `Pleroma`_.

.. _Mastodon: https://joinmastodon.org/
.. _Pleroma: https://pleroma.social/
