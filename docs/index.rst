Minimal-Activitypub
===================


Welcome to the minimal-activitypub documentation! Minimal-activitypub is a an ActivityPub api implementation
in Python. Most methods are using asyncio. Only a limited number of API methods are implemented. Basically
just the methods needed for my other fediverse related projects, such as `Fedinesia`_, `Lemmy2Fedi`_ and
`Tootbot`_.

.. _Tootbot: https://pypi.org/project/tootbot/
.. _Fedinesia: https://pypi.org/project/fedinesia/
.. _Lemmy2Fedi: https://codeberg.org/MarvinsMastodonTools/lemmy2fedi


.. toctree::
   :maxdepth: 2

   authentication
   posting

   api-methods

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
